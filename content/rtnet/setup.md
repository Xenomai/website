---
menuTitle: "Setup"
title: "Setting up RTnet"
weight: 103
pre: "&#9702; "
---

This page describes the steps for setting up the RTnet stack shipped
with Xenomai 3. This is intended as a guide to the beginner, multiple
deviations to these instructions can be implemented for custom
installations.

## Testing with a single node (local loopback)

The testing procedure is as follows:

-   Disconnect the RTnet node’s network cable from your normal non
    real-time ehternet network

-   Restart the systems into the patched kernel mode.

-   Display the Network setup with the command

<!-- -->

    $ ifconfig

This will show you have the eth0 running (which is your network card)
and the lo running (which is the local loop-back)

-   You need to disable the non-realtime network operation of the
    network card as follows:

<!-- -->

    $ ifconfig eth0 down

-   Then, unload the network card’s device drivers, e.g.

<!-- -->

    $ sudo rmmod 8139too

-   Load the required modules of your real-time Linux extension needed
    for RTnet operations. If you have built the RTDM support of Xenomai
    2.x into the kernel, or you are running Xenomai 3.x, nothing has to
    be done. Otherwise issue:

-   Edit the following parameters in the [RTnet configuration
    file]({{< relref "rtnet/config.md" >}}) located at
    */usr/local/rtnet/etc/rtnet.conf*:

    1.  Set the host up as master or slave depending on how you are
        going to use it.

    2.  The RT\_DRIVER should be the realtime equivalent of the module
        you removed nl. rt\_8139too.

    3.  RT\_DRIVER\_OPTIONS should just have empty quotes because there
        is only one card on the Host.

    4.  IPADDR = 127.0.0.2 because you setup yourself as the master with
        this address and you want to talk to the local loopback which
        will be setup as a slave at 127.0.0.1

    5.  NETMASK = 255.255.255.0

    6.  RT\_LOOPBACK = yes

    7.  RTCAP = yes if you want to run real time capturing of packets
        like Ethereal.

    8.  Setup TDMA\_SLAVES = 127.0.0.1 for loopback as the slave.

-   Save the file and from the *sbin/* directory, and run:

<!-- -->

    $ sudo ./rtnet start

-   It will wait for slaves. press cntl ^C to go out of its waiting for
    slaves. You can see that RTnet is running by calling `lsmod` and
    finding the realtime driver of the network card in the loaded module
    list. If it is not listed then RTnet is not running.

-   Ping the local loopback as follows:

<!-- -->

    $ sudo ./rtping 127.0.0.1

-   To stop RTnet transmitting on the card, run:

<!-- -->

    $ sudo ./rtnet stop

## Testing with multiple nodes

The testing procedure is as follows:

-   Disconnect the RTnet nodes from the normal non real-time ethernet
    network.

-   Connect the RTnet nodes to each other using a switch (or hub).

-   Restart the systems into the patched kernel mode.

-   Modify the rtnet configuration file
    */usr/local/rtnet/etc/rtnet.conf*. You should check the following:

1.  RT\_DRIVER must be set to the correct real-time Ethernet NIC driver

2.  Set IPADDR to the station’s (network-unique) IP address

3.  Set TDMA\_MODE to *master* or *slave*, depending on the node’s role

4.  Place the slaves' IP addresses in TDMA\_SLAVES, separated by spaces
    (only required by master)

-   Shut down the non-real time Ethernet device

-   Unload the non-real time Ethernet device driver modules

-   Load the required Xenomai modules

-   Change to directory /usr/local/rtnet/sbin

-   Start RTnet on all nodes. The nodes should see each others, and then
    return to the command prompt:

<!-- -->

    $ sudo ./rtnet start

-   Check the communication between the nodes with the following
    command, where is the IP address of a remote node:

<!-- -->

    $ sudo ./rtping

Stop the command with ^C.

-   Unload the RTnet modules:

<!-- -->

    $ sudo ./rtnet stop

## Debugging RTnet

If RTnet doesn’t work, or only works intermittently, it is likely due to
one of these known issues:

-   Xenomai is not working as expected on your hardware.

-   The Linux driver for the real-time network device was built into the
    kernel and blocks the hardware.

-   IRQ clash. Xenomai is able to detect conflicts and report them to
    the kernel console.

-   you are using rt\_e1000 driver and RTnet 0.9.9 or older. See
    RTnet:rt\_e1000 for a workaround.

Steps to resolve problems:

-   Check the kernel console or the system log for suspicious messages.

-   Validate that the basic tests of your real-time extension are
    working correctly. Xenomai, e.g., comes with an basic test called
    `latency`.

-   Collect informations about your setup (versions, configuration,
    output messages) and post a support request on
    [rtnet-users](http://sourceforge.net/mail/?group_id=68976).

## Using realtime (RTnet) and non-realtime network together

If you have two (or more) ethernet lines available on your system, you
can configure one to use RTnet (realtime) and the other one to use
non-realtime network:

-   Unload the non-realtime driver.

-   Load and start rtnet on **one** card using *cards* parameter (e.g.
    `insmod rt_e1000.ko cards=1,0`).

-   Load the non-realtime module and bring it up (the standard way using
    insmod and ifup). It will take whatever cards are left.

The parameter *cards* accepts an array of zeros and ones. For instance
`insmod rt_e1000.ko cards=0,1,0` will use the "middle" card of 3.

See script fragment below for an example:

    $ sudo insmod /usr/local/rtnet/modules/rt_e1000.ko cards=1,0
    $ sudo insmod e1000 $ sudo ifup eth1
