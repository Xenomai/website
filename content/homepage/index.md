---
headless: true
title: homepage
---

<div class="home-grid-container">

  <div class="signature-item">
       <a href="#">{{< homeimg src="images/signature.png" alt="" >}}</a>
  </div>

  <div class="overview-item">
      {{% homebutton href="/overview/" icon="fas fa-play" %}}Get started{{% /homebutton %}}
  </div>
 
  <div class="repo-table-item">
      <style>
      #coderef {
	       width: 650px;
      }
      #coderef td {
      	       border-style: none;
	       padding: 2px;
  	       color: #fff;
  	       text-align: center;
      }
      #coderef th {
	       padding: 8px;
  	       text-align: center;
    	       background-color: #fff;
  	       color: #0084ba;
      }
      a:link {
      	 color: #fff;
      }
      a:active {
      	 color: #fff;
      }
      a:visited {
      	 color: #fff;
      }
      a:hover {
      	 color: #00d2ed;
      }
      </style>
      <table id="coderef">
	<col width="40">
      	<col width="25%">
      	<col width="35%">
      <tr>
        <th>Repository</th>
        <th>Branch</th>
        <th>Latest</th>
      </tr>
      <tr>
        <td><a href="https://git.xenomai.org/linux-dovetail.git">linux-dovetail (LTS)</a></td>
        <td><a href="https://git.xenomai.org/linux-dovetail/-/tree/{{< param dovetailLTSBranch >}}-dovetail">{{< param dovetailLTSBranch >}}</a></td>
        <td><a href="https://git.xenomai.org/linux-dovetail/-/tree/{{< param dovetailLTSLatest >}}">{{< param dovetailLTSLatest >}}</a></td>
      </tr>
      <tr>
        <td><a href="https://git.xenomai.org/linux-dovetail.git">linux-dovetail (SLTS)</a></td>
        <td><a href="https://git.xenomai.org/linux-dovetail/-/tree/{{< param dovetailSLTSBranch >}}-dovetail">{{< param dovetailSLTSBranch >}}</a></td>
        <td><a href="https://git.xenomai.org/linux-dovetail/-/tree/{{< param dovetailSLTSLatest >}}">{{< param dovetailSLTSLatest >}}</a></td>
      </tr>
      <tr>
        <td><a href="https://git.xenomai.org/xenomai">Cobalt LTS</a></td>
        <td><a href="https://git.xenomai.org/xenomai/-/tree/{{< param cobaltStable >}}">{{< param cobaltStable >}}</a></td>
        <td><a href="https://git.xenomai.org/xenomai/-/tags/{{< param cobaltLTS >}}">{{< param cobaltLTS >}}</a></td>
      </tr>
      <tr>
        <td><a href="https://git.xenomai.org/xenomai">Cobalt latest</a></td>
        <td><a href="https://git.xenomai.org/xenomai/-/tree/master">master</a></td>
        <td><a href="https://git.xenomai.org/xenomai/-/tree/master/{{< param cobaltLatest >}}">{{< param cobaltLatest >}}</a></td>
      </tr>
      </table>
  </div>

</div>
