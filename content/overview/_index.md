---
title: "Overview"
weight: 1
pre: "&#8226; "
---

Xenomai 3 is a real-time framework which can run seamlessly
side-by-side Linux as a dual kernel system, or natively over mainline
Linux kernels. In the latter case, the mainline kernel can be
supplemented by the [PREEMPT-RT
patch](https://www.kernel.org/pub/linux/kernel/projects/rt/) to meet
stricter response time requirements than standard kernel preemption
would bring.

One of the two available real-time cores is selected at build time. The
dual kernel core is codenamed *Cobalt*, the native Linux implementation
is called *Mercury*.

If you are looking for detailed information about installing the
legacy Xenomai 2.x series, please refer to [this document]({{< relref
"legacy/_index.md" >}}) instead. Please note that Xenomai 2.x is
discontinued and not maintained anymore.

## How does Xenomai 3 deliver real-time?

There are two options:

-   By supplementing Linux with a real-time core running side-by-side
    with it. This small extension named **Cobalt** is built into the
    Linux kernel, dealing with all time-critical activities, such as
    handling interrupts, and scheduling real-time threads. The Cobalt
    core has higher priority over the native kernel activities.

    In this *dual kernel* configuration, all the RTOS APIs Xenomai 3
    provides interface with the Cobalt core, and only those APIs are
    deemed real-time capable, including the subset of POSIX 1003.1c
    services implemented by Xenomai 3 (aka *libcobalt*).

![Xenomai 3 dual kernel configuration](/images/x3-cobalt-interfaces.png)

Cobalt is an evolution of the legacy Xenomai 2 architecture.

-   By relying on the real-time capabilities of the native Linux kernel,
    forming the **Mercury** core. Often, applications will require the
    [PREEMPT-RT](https://www.osadl.org/Realtime-Linux.projects-realtime-linux.0.html)
    extension to be enabled in the target kernel, for delivering
    real-time services.

    However, this is not mandatory, it depends on the application
    requirements with respect to responsiveness and maximum jitter; some
    may even tolerate a certain percentage of deadline misses.

    In this *single kernel* configuration, all the non-POSIX RTOS APIs
    Xenomai 3 provides are accurately emulated over the native
    threading library (preferably **NPTL**, but also supports
    **linuxthreads** for legacy setups).

![Xenomai 3 single kernel configuration](/images/x3-mercury-interfaces.png)

Xenomai 3 supports both the single and dual kernel configurations. The
discontinued Xenomai 2 architecture only supported the dual kernel
configuration.

## Supported hardware

Xenomai 3 is supported on a variety of hardware. The ports we know
about are [listed here]({{< relref "hardware/_index.md" >}}).

## Xenomai 3 license terms

All Xenomai 3 code running in kernel space is licensed under the terms
of the Linux kernel license, i.e. GPL v2. Xenomai libraries linked to
applications are licensed under the terms of the LGPL v2.1. For
information regarding a particular software component, you should look
at the COPYING file available in the directory containing the relevant
source code.

## Preparation checklist

### Single or dual kernel configuration?

Assuming you know the target SoC and the application requirements with
respect to real-time guarantees, you should define which of the single
or dual kernel configurations best fits your needs. The following hints
may help:

-   Does your application actually have real-time requirements, and how
    stringent are they? If migrating an application from an *embedded*
    RTOS to a Linux-based platform, the question may stand because
    unlike legacy RTOSes, Linux sees embedded and real-time
    characteristics as orthogonal issues. Some migrated applications may
    even be fine with only emulating the original RTOS API over a
    regular kernel and standard preemption (CONFIG\_PREEMPT). Those may
    benefit from Xenomai 3 in single kernel configuration.

    -   For stringent real-time requirements, you should consider the
        availability of the hard real-time support for your target SoC
        architecture and Linux kernel version.

        -   Hardware support for Xenomai 3 in dual kernel
            configuration can be viewed
            [here]({{< relref "hardware/_index.md" >}}). This approach is based on a
            kernel software layer which introduces a mechanism for
            diverting all critical events to a dual kernel extension
            core, coupled to the host Linux kernel. This mechanism
            called Dovetail implements an *Interrupt pipeline*.

        -   For a single kernel configuration, the list of target
            architectures supporting the full preemption features (aka
            *PREEMPT-RT*) is available
            [here](https://www.osadl.org/Realtime-Linux.projects-realtime-linux.0.html).

    -   How many CPU cores will be involved in real-time operations?

        -   in a dual kernel configuration, the Xenomai 3 Cobalt core
            normally benefits from its simpler locking scheme when
            dealing with real-time activities concurrently on not more
            than four CPU cores.

        -   Beyond four CPU cores running real-time activities, SMP
            scalability will be better with a single kernel
            configuration.

The key issue is with the number of CPU cores **actually running
real-time threads** and receiving interrupts from real-time sources,
not with the overall number of cores on line on the target
hardware. Because the Cobalt cocore does not share any lock with the
regular Linux kernel when dealing with real-time activities, a 16-way
server pinning such activities on not more than four of the available
cores would still deliver good performances in a Cobalt-based dual
kernel configuration.

{{% notice info %}}
If you need a SMP scalable real-time core, then you may want to
consider [Xenomai 4](https://v4.xenomai.org/) instead. The EVL core
underlying Xenomai 4 implements a fine-grained locking model which
does not suffer the scalability issue of its predecessor.
{{% /notice %}}

-   Is any of those dual kernel pros critical to your case?

    -   porting the Cobalt core to a new architecture is fairly
        simple.

    -   it is decoupled from the development cycle of the mainline Linux
        kernel, thus allowing to pick the desired (or required) kernel
        version more freely.

    -   it can simplify the worst-case analysis and keeps the results
        valid over update cycles of the mainline Linux kernel which
        hosts it.

    -   it allows to fine-tune the non-real-time part for throughput
        without negative impact on the real-time job.

    -   it does not require any tuning of the regular Linux system to
        guarantee short and bounded latencies for the real-time job.

-   Is any of those single kernel pros critical to your case?

    -   the stock kernel drivers can be reused by real-time applications
        with no change (provided they don’t create latency issues due to
        an unfortunate implementation). On the other hand, drivers need
        to be specifically implemented for running over a dual kernel
        configuration, i.e. over the in-kernel RTDM interface.

    -   the programming model may be simpler than with a dual kernel
        configuration, since the kernel is deemed to enforce real-time
        behavior globally. This said, cautious application design and
        implementation must be a common rule regardless of the
        underlying real-time technology, as not all available services
        may be suited for real-time usage.

    -   all standard utilities for monitoring the system operations can
        report about the real-time activities out of the box. On the
        other hand, a dual kernel system has to adapt such tools
        specifically for this purpose.

### Do you need non-POSIX real-time APIs?

Porting legacy non-POSIX applications to POSIX may not be the best
option. Although there may be similarities, the semantics of traditional
RTOS APIs may differ significantly from POSIX in many cases, which often
makes the migration process quite painful and error-prone.

Xenomai 3 ships with emulators of traditional RTOS APIs, like VxWorks™
and pSOS™, both available in single and dual kernel
configurations. Xenomai 3 aims at accurately mimicking the original
services with low overhead.

However, maybe Xenomai 3 does not provide an emulator yet for the API
you are looking for, or some services are missing from the existing
emulators, in which case you should consider raising the topic on the
[e-mail discussion list](mailto:xenomai@lists.linux.dev). Xenomai 3 is
based on generic building blocks for implementing RTOS APIs, so
extending it is a documented option.

#### Using GIT

The Xenomai project uses [GIT](https://git-scm.com/) for managing the
source code repositories. There is no shortage of GIT crash courses,
references, howtos, tips and comprehensive documentation available from
the Internet. For the sake of conciseness, we will only mention two
documentation hubs from which you should be able to find your way:

-   [The GIT project documentation hub](https://git-scm.com/documentation).

-   [The GIT documentation wiki](https://git.wiki.kernel.org/index.php/GitDocumentation).

In addition, let’s mention a simple tutorial for bootstrapping with GIT
and day-to-day usage:

<https://www.kernel.org/pub/software/scm/git/docs/gittutorial.html>

## Building Xenomai 3 from sources

[This document]({{< relref "installation/_index.md" >}}) contains
instructions for building and installing Xenomai 3.x.

## Xenomai-3 development tree

The Xenomai 3 development takes place in the
[xenomai](https://git.xenomai.org/xenomai) repository of this [Gitlab
instance](https://git.xenomai.org) operated by [Denx Software
Engineering](https://denx.de), a long-time contributor to the Xenomai
project.

- the _master_ branch tracks the development HEAD toward the next
  major release. This branch is never rebased.

- the _next_ branch tracks the latest commits pending merge into the
  _master_ branch, it contains the most bleeding edge work. This
  branch *may be rebased* until it gets merged into the _master_
  branch eventually.

- _stable-*_ branches track updates to stable releases, such as
  stable-3.2.x which is the maintenance branch for the v3.2 major
  release. Those branches are never rebased.

## If something goes wrong

First and foremost, please make sure to have a look at the
[troubleshooting guide]({{< relref "troubleshooting/_index.md" >}}).

If running any release from the discontinued Xenomai 2 series, or a
Xenomai 3 release using the **Cobalt** real-time core, then you are
running a dual kernel configuration. Xenomai 3 over the **Mercury** core
stands for a single kernel configuration.

If still out of luck, and if your favorite Internet search engine did
not help finding a similar/same problem already solved for another user,
then you should try raising the issue on the [main e-mail discussion
list](mailto:xenomai@lists.linux.dev).

To maximize your chances of receiving quick and accurate responses to
your request for help, you really want to follow [these
recommendations]({{< relref "tips/getting-help.md" >}}).

## Embedded Linux distributions & Xenomai

The Yocto-based [Embedded Linux Development
Kit](https://www.denx.de/wiki/DULG/ELDK) (aka *ELDK*) includes
pre-built GNU cross-development tools and bootable root file systems
for a variety of ARM™, PowerPC™ and MIPS™ embedded target systems.  It
also ships with a pre-built stable Xenomai release.
