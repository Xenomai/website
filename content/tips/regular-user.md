---
title: "Running as regular user"
weight: 80
pre: "&#9702; "
---

You can allow non-root users to access Xenomai 3 services from user
space. You only have to provide the ID of a unix group whose members
shall obtain this right plus additional Linux capabilities required to
work with Xenomai 3. To do so:

-   specify the module parameter `xenomai.allowed_group=<gid>` on the
    kernel command line as or

-   write it into sysfs
    (`echo "<gid>" > /sys/module/xenomai/parameters/allowed_group`)

In addition, check that /dev/rtpipe belongs to the correct group. The
Xenomai-provided udev scripts assume that there is a group called
*xenomai*, you may have to adjust this according to the local
configuration.

Don’t believe that this mechanism allows to run Xenomai applications in
whatever securely confined way! We grant CAP\_SYS\_RAWIO to all Xenomai
users, some Xenomai services can easily be corrupted/exploited from user
space (those based on shared heaps e.g.), and no one audits the core or
all the drivers for security. The advantage of having a separate Xenomai
group instead of just assigning root access directly is being able to
avoid *accidental* changes, nothing more!
