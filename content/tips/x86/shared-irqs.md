---
title: "Dealing with shared IRQs"
weight: 75
---

IRQ sharing between RT (Xenomai real-time core) and non-RT (main Linux
kernel) domains is fragile regardless of the CPU architecture, can
easily destroy determinism on the RT side, and should therefore be
avoided. However, such sharingis more prone to happen on legacy x86
platforms.  Sharing IRQs between RT devices is not problematic, but
still comes with additional overhead compared to dedicated IRQ lines.

On x86, the following approaches exist to untangle devices IRQ-wise:

-   If your hardware implements the Message Signaled Interrupts (MSI),
    enabling CONFIG\_MSI in the kernel should fix the issue.

-   If at least one of the devices is a PCI card, try flipping its
    position on the motherboard.

-   PC104+ cards sometimes have jumpers (yeah, they still exist) to
    change the IRQ line. Otherwise, their ordering in the PC104 stack
    also influences the IRQ line

-   Some (but not many) PC and notebook BIOSes allow to change the IRQ
    assignment.

-   Disable the conflicting Linux device (e.g. in the BIOS) or simply
    don’t load its driver.

-   Disable some other on-board device that is unneeded. Sometimes this
    helps to persuade the system to avoid shared IRQs.

-   Enable CONFIG\_X86\_LOCAL\_APIC or CONFIG\_X86\_IO\_APIC, it
    increases the number of IRQs and may help (though nobody ever
    reported success with this).

-   Ask you board vendor about possibilities to untangle IRQ lines — or
    beat some engineer of your hardware design team if it is a homegrown
    problem.

-   In case nothing works, and for a given configuration, you may modify
    the non real-time driver which shares an IRQ with a real-time driver
    as indicated in [this post](https://lore.kernel.org/xenomai/489026E5.1010801@domain.hid/).
