---
title: "Configuring for x86"
weight: 73
pre: "&#9702; "
---

On a non-x86 embedded platform, configuring the Linux kernel is usually
easy: there is a default configuration file for the SoC where you should
just start from, set the Xenomai options as you see fit, possibly tweak
a few bits here and there, and you are done.

Configuring x86 kernels is a bit different, there is no default
configuration file for each of the PC models around, and the only kernel
configuration file you may want to start from is shipped with your
distribution kernel.

Unfortunately, the typical one-fits-it-all configuration shipped with
any desktop/server distribution is always conflicting with real-time
requirements these days, particularly with dual kernel configurations.

In this section, you will find a few guidelines in order to set up a
kernel for Xenomai in a dual kernel configuration for x86.

{{% notice warning %}}
Use care when referring to old mail posts, archives, and your favorite
search engine results.
If you find some mails in our archives saying "please disable option X
or Y", please do not take these advices as a rule of thumb for the
common case. These are most often attempts to nail down a problem in
some user setup, and turning particular options on/off is part of the
process.
For this reason, always make sure to take similar advices found on
third-party web sites with a grain of salt, as the author might have
mis-interpreted such information the same way. If in doubt, you should
ask on the [Xenomai discussion
list](https://subspace.kernel.org/lists.linux.dev.html) for
confirmation.
{{% /notice %}}
