---
title: "Legacy"
weight: 1000
pre: "&#8226; "
---

**Xenomai 2 is EOL. The information in this section is kept accessible
for archiving purpose only.**

{{% notice warning %}}
Please do NOT install Xenomai 2. If you are looking for the latest
real-time core architecture from the Xenomai family, you may want to
consider [Xenomai 4](https://v4.xenomai.org/) instead.
{{% /notice %}}
