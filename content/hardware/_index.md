---
menuTitle: "Hardware"
title: "Supported hardware"
weight: 60
pre: "&#8226; "
---

This section provides information regarding which embedded hardware is
known to work with Xenomai 3 over the **Cobalt** core, a real-time
core running alongside Linux on the same machine. Xenomai 3 ports in
dual-kernel configuration are based on the mainline Linux kernel which
can be found at
git://git.kernel.org/pub/scm/linux/kernel/git/torvalds/linux-2.6.git.

The interrupt pipeline support (aka **Dovetail**) must be available
from the target kernel, for interfacing Linux with the **Cobalt**
core.  Cobalt requires a Linux 3.10 kernel or later.  The information
below may be incomplete and/or outdated; the Xenomai project is not
committed to support each and every SBC/platform from each and every
architecture in all Xenomai releases, and new hardware support is
regularly added to this list. If unsure about whether your hardware is
supported by a given Xenomai release, ask on the [Xenomai mailing
list](mailto:xenomai@lists.linux.dev).

## Supported CPU architectures

### ARM

#### Supported SoC

-   **Broadcom**

    -   BCM2835

-   **Freescale**

    -   i.MX family

        -   i.MX6 series

        -   i.MX7 series

    -   QorIQ LS1 family

-   **STMicroelectronics**

    -   STIH4x

-   **Texas Instruments**

    -   OMAP3 family

    -   OMAP4 family

    -   AM33xx family

    -   AM5726

-   **Xilinx**

    -   Zynq family

-   **Altera**

    -   Cyclone V (SoC FPGA)

#### Discontinued SoC support (since kernel 4.14)

-   **ARM**

    -   Integrator/CP

-   **Atmel**

    -   at91 family

-   **Freescale**

    -   i.MX family

        -   i.MX1

        -   i.MX21

        -   i.MX27

        -   i.MX28

        -   i.MX31

        -   i.MX51

        -   i.MX53

-   **Intel**

    -   ixp4xx

-   **Intel/Marvell**

    -   PXA family

    -   SA11x0-based

-   **Samsung**

    -   S3C24xx family

-   **STMicroelectronics**

    -   SPEAr600

#### Supported Evaluation Boards

-   **Freescale**

    -   i.MX6: Solo

    -   i.MX6Q: SabreSD, SabreLite

    -   i.MX6Q: PhyFLEX

    -   i.MX7D: SDB

    -   QorIQ LS1: LS1021

-   **ISEE**

    -   OMAP3530: IGEP v2

-   **Raspberry Pi foundation**

    -   Pi Zero

    -   Pi 2 Model B (multi\_v7\_defconfig, bcm2836-rpi-2-b.dtb)

    -   Pi 3 Model B (32 bit mode, multi\_v7\_defconfig,
        bcm2837-rpi-3-b.dts)

-   **Texas Instrument**

    -   OMAP3530: Beagle

    -   OMAP4430: Panda

    -   AM33xx: BeagleBone

-   **VIA**

    -   IMX6Q: VAB-820 Pico-ITX

#### Discontinued EVB support (since kernel 4.14)

-   **Balloonz Ltd**

    -   SA1110: Balloon2

-   **Calao Systems**

    -   AT91SAM9263: USB-A9263

-   **Cogent Computer**

    -   i.MX21: CSB535fs

    -   AT91RM9200: CSB637

-   **DAVE s.r.l. / DENX Computer Systems GmbH**

    -   i.MX31: Qong EVB-Lite

-   **Freescale**

    -   i.MX1: MX1ADS

    -   i.MX31: MX31ADS

    -   i.MX51: MX51 Babbage

-   **Intel**

    -   IXP465: IXDP465

-   **Linksys/Cisco**

    -   IXP425: NSLU2

-   **STMicroelectronics**

    -   EVALSPEAr600

### AARCH64

#### Supported SoC

-   **Hisilicon**

    -   Kirin 620

-   **Broadcom**

    -   BCM2836

#### Supported Evaluation Boards

-   **Hisilicon**

    -   HiKey (96Boards Consumer Edition CircuitCo/LeMaker)

-   **Raspberry Pi foundation**

    -   Pi 3 Model B (64 bit mode)

### PowerPC/32

#### Supported Architectures

Xenomai 3 was reported to work on platforms based on the following
embedded PowerPC32 architectures: 40x, 44x, 512x, 52xx, 82xx, 83xx,
85xx.

#### Supported Evaluation Boards

Xenomai 3 is known to work on those PowerPC-based Evaluation Boards:

-   **AMCC**

    -   PPC405EX: Kilauea, Makalu

    -   PPC405GP: Walnut

    -   PPC405GPr: Sycamore

    -   PPC405EP: Bubinga, Taihu

    -   PPC440EP: Bamboo, Yosemite

    -   PPC440EPx: Sequoia

    -   PPC440GX: Ocotea, Taishan

    -   PPC440GR: Yellowstone

    -   PPC440GRx: Rainier

    -   PPC440SPe: Yucca, Katmai

-   **Avnet**

    -   V5FX30T \[ Virtex-5 XC5VFX30T FPGA with ppc440 (400 MHz) \]

-   **DAVE s.r.l. / DENX Computer Systems GmbH**

    -   MPC5121: Aria

-   **Freescale**

    -   MPC5121-ADS

    -   MPC8272-ADS

    -   MPC8313E-RDB

    -   MPC8349-ITX

    -   MPC8349E-MITX-GP

    -   MPC8360-MDS

    -   MPC8540-ADS

    -   MPC8545-CDS

    -   MPC8548E-CDS

    -   MPC5200, MPC5200b: Lite5200, Lite5200b

    -   QorIQ P2020

-   **TQ Components**

    -   MPC8548E: TQM8548

    -   MPC8560: TQM8560

-   **Xilinx**

    -   ML403 \[ Virtex-4 XC4VFX12 FPGA with ppc405 (300 MHz) \]

    -   ML507 \[ Virtex-5 XC5VFX70T FPGA with ppc440 (400 MHz) \]

### x86

All x86-compatible 32/64bit processor types starting from i586 CPUs with
a TSC are supported. Lower latencies are achieved when a local APIC is
available from the CPU.

Make sure the processor type you pick in your kernel configuration
actually matches your CPU. In case you get weird latencies when running
Xenomai 3 on your x86 board, make sure to check out this
[information]({{< relref "tips/x86/common.md" >}}).

## Discontinued CPU architecture support

### Nios II (up to kernel 2.6.35, Xenomai 2.6.x series)

#### Discontinued Nios II board support

-   **Altera**

    -   DE2

    -   DE2-70

    -   Cyclone III 3C120

    -   Cyclone III 3C25

    -   Cyclone V

SH-4 (up to kernel 2.6.35, Xenomai 2.6.x series)

    Discontinued SH-4 board support
    ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

    * ST-Microelectronics
    ** STMicro MB442

    Blackfin (up to kernel 4.14, Xenomai 3.0.x series)

#### Discontinued Blackfin processors support

-   **Analog Devices**

    -   BF526

    -   BF527

    -   BF533

    -   BF537

    -   BF548

    -   BF561

    -   BF609

#### Discontinued Blackfin board support

-   **Analog Devices**

    -   BF526 EZ-KIT

    -   BF527 EZ-KIT Lite

    -   BF533 STAMP

    -   BF537 STAMP

    -   BF548 EZ-KIT Lite

    -   BF561 EZ-KIT

    -   BF609 EZ-KIT

### PowerPC/64 (up to kernel 4.14, Xenomai 3.0.x series)

#### Discontinued ppc64 architecture support

-   86xx

-   pa6t

#### Discontinued ppc64 board support

-   **Emerson**

    -   MPC8641D: MVME7100

-   **PA-Semi**

    -   PA6T: 1682M Electra

-   **Freescale**

    -   QorIQ T1042
